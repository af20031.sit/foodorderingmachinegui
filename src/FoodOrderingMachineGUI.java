import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodOrderingMachineGUI {
    private JPanel root;
    private JButton steakButton;
    private JButton sushiButton;
    private JButton eelRiceBoxButton;
    private JButton yakitoriButton;
    private JButton omeletteRiceButton;
    private JButton grilledSalmonLunchSetButton;
    private JButton checkOutButton;
    private JTextPane textPane1;
    private JLabel Total;
    private JButton cancelOrderButton;
    private JButton freeDrinksButton;

    private int sum = 0;

    void order(String food, int price){
        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like to order "+food+"?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);
        if(confirmation == 0){
            String currentText =textPane1.getText();
            textPane1.setText(currentText+food+":"+price +"yen\n");
            JOptionPane.showMessageDialog(
                    null,
                    "Thank you for ordering "+food+ " It will be served as soon as possible."
            );
            sum += price;
            Total.setText("Total : " + sum + " yen.");
        }
    }

    public FoodOrderingMachineGUI() {
        String currentText = steakButton.getText();
        steakButton.setIcon(new ImageIcon(this.getClass().getResource("steak.jpg")));
        sushiButton.setIcon(new ImageIcon(this.getClass().getResource("sushi.jpg")));
        eelRiceBoxButton.setIcon(new ImageIcon(this.getClass().getResource("eel.jpg")));
        yakitoriButton.setIcon(new ImageIcon(this.getClass().getResource("yakitori.jpg")));
        omeletteRiceButton.setIcon(new ImageIcon(this.getClass().getResource("omelette.jpg")));
        grilledSalmonLunchSetButton.setIcon(new ImageIcon(this.getClass().getResource("grilledsalmon.jpg")));

        steakButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Steak",1500);
            }
        });
        sushiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Sushi",600);
            }
        });
        eelRiceBoxButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Eel Rice Box",3000);
            }
        });
        yakitoriButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Yakitori",300);
            }
        });
        omeletteRiceButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Omelette Rice",500);
            }
        });
        grilledSalmonLunchSetButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Grilled Salmon",250);
            }
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(
                        null,
                        "Would you like to checkout?",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if(confirmation==0){
                    JOptionPane.showMessageDialog(
                            null,
                            "Thank you.The total price is " + sum + " yen."
                    );
                    sum=0;
                    textPane1.setText("");
                    Total.setText("Total :   0 yen.");
                }
            }
        });
        cancelOrderButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation2 = JOptionPane.showConfirmDialog(
                        null,
                        "Would you like to cancel order?",
                        "Cancel Order Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if(confirmation2==0){
                    JOptionPane.showMessageDialog(
                            null,
                            "Cancel your order."
                    );
                    sum=0;
                    textPane1.setText("");
                    Total.setText("Total :   0 yen.");
                }
            }
        });
        freeDrinksButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation3 = JOptionPane.showConfirmDialog(
                        null,
                        "How about free water?",
                        "Free Drink Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if(confirmation3==0){
                    JOptionPane.showMessageDialog(
                            null,
                            "Thank you for ordering water! It will be served as soon as possible."
                    );
                    String currentText =textPane1.getText();
                    textPane1.setText(currentText+"Water: 0 yen\n");
                }
            }
        });
    }
    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodOrderingMachineGUI");
        frame.setContentPane(new FoodOrderingMachineGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
